module.exports = {
    publicPath: process.env.NODE_ENV === 'production' && !process.versions['electron']
        ? '/markeditor'
        : '/'
}
