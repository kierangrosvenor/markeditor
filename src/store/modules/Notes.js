import {Note} from '../../entities/Note'

const uuidv4 = require('uuid/v4');

let content = `

# :smile: Welcome to Mark Editor
Probably not the last markdown editor...

#### Features
*  GitHub emoji support :fire:
*  Toolbar with __actions__ to modify text selection :smile:
    - Heading 
    - Bold
    - Italics
    - Strike throughs
    - ~~Underlines~~
*  Resizable pane :tada:
*  Runs in browser and electron :rocket:
*  Undo 

#### Roadmap

* Table creation UI
* Persist Markdown sessions with an Authentication service / user accounts


### Tables
You can **create** tables with Mark Editor using this syntax

\`\`\`

| h1    |    h2   |      h3 |
|:------|:-------:|--------:|
| 100   | [a][1]  | ![b][2] |
| *foo* | **bar** | ~~baz~~ |

\`\`\`
| h1    |    h2   |      h3 |
|:------|:-------:|--------:|
| 100   | [a][1]  | ![b][2] |
| *foo* | **bar** | ~~baz~~ |

## Dependencies

* Electron
* VueJS
* Brace
* Showdown
* jQuery
* Twitter Bootstrap 4
* Google Material Icons


`;

const state = {
    activeNote: null,
    notes: [
        {
            guid: 'xxx',
            title: 'Welcome to Mark Editor',
            content: content
        }
    ]
};

/**
 *
 * @type {{addNote(*, Note): void, updateNote(*, Note): void, deleteNote(*, string): void}}
 */
const mutations = {
    setActiveNote(state, value: Note): void {
        state.activeNote = value;
    },
    /**
     * Add a note
     *
     * @param state
     * @param value
     */
    addNote(state, value: Note): void {
        let notes = JSON.parse(JSON.stringify(state.notes));
        value.uuid = uuidv4();
        value.created = new Date();
        notes.unshift(JSON.parse(JSON.stringify(value)));
        state.notes = notes;
        state.activeNote = value;
    },
    /**
     * Update a note
     *
     * @param state
     * @param value
     */
    updateNote(state, value: Note): void {
        let notes = JSON.parse(JSON.stringify(state.notes));
        let note: Note = notes.find(x => x.uuid === value.uuid);
        let noteIndex = notes.findIndex(x => x.uuid === value.uuid);

        if (note) {
            note.title = value.title ? value.title : note.title;
            note.content = value.content ? value.content : note.content
        }

        notes[noteIndex] = note;
        state.notes = notes;
    },
    /**
     * Delete a note by uuid
     *
     * @param state
     * @param uuid
     */
    deleteNote(state, uuid: string): void {
        let index: number = state.notes.findIndex(x => x.uuid === uuid);
        state.notes.splice(index, 1);
    }
};

const getters = {
    getNotes(state): Array<Note> {
        return state.notes;
    },
    getActiveNote(state): Note {
        return state.activeNote;
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters
}