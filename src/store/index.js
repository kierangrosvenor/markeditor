import Vue from 'vue'
import Vuex from 'vuex'

import VuexPersistence from 'vuex-persist'
import localforage from 'localforage'

const vuexLocal = new VuexPersistence({
    storage: localforage
});

import {Notes} from './modules'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        Notes
    },
    plugins: [
        vuexLocal.plugin
    ]
})
