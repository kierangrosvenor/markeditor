export class Note {
    uuid: string = null;
    title: string = null;
    content: string = null;
    created: Date = null;
}